### Yet another Openweathermap application... 

## Todo:

- Refactor and cleanup.
- Remove hardcoded suggestion list.
- Styling.
- etc.etc.

## Available at:

http://yet-another-weatherapp.herokuapp.com/

###

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).