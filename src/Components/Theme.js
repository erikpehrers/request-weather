const breakpoints = [576, 768, 992, 1200];
const media = breakpoints.map(
  breakpoint => `@media screen and (max-width: ${breakpoint}px) {`
);

export const styleReset = `
html {
  font-size: 16px;
}
html * {
  box-sizing: border-box;
}

body {
  height: 100vh;
  font-family: 'Source Sans Pro', sans-serif;
  color: #222222;
  overflow: scroll;
  background: #efefef;
}
`;

const theme = {
  black: '#383838',
  primary: 'rgba(18, 43, 59, 1)',
  secondary: 'rgba(18, 43, 59, .5)',
  active: 'rgba(18, 43, 59, 0.8)',
  inactive: 'rgba(18, 43, 59, 0.2)',
  fontBold: '900',
  fontRegular: '400',
  media: media
};

export default theme;

// background-image: linear-gradient(to right, #3cbfc4, #75bb94, #a9b076, #cea279, #db9896);
