import React from 'react';
import styled from '@emotion/styled';

// Use buttons for a11y
const IconHolder = styled.button`
  font-size: 2.5rem;
  grid-area: ${p => p.placement};
  align-self: center;
  justify-self: center;
  cursor: pointer;
  outline: 0;
  background: transparent;
  border: 0;
`;

const Icon = ({ children }) => {
  return <IconHolder>{children}</IconHolder>;
};

export default Icon;
