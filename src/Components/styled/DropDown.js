import styled from '@emotion/styled';

export const Dropdown = styled.ul`
  width: 100%;
  padding: 0;
  list-style: none;
  grid-area: result;
  margin: 0;
`;

export const DropdownItem = styled.li`
  line-height: 65px;
  font-size: 1.5rem;
  cursor: pointer;
  letter-spacing: 1px;
  padding: 0 1.5rem;
  background: ${p => (p.isHighlighted ? 'rgba(18, 43, 59, 0.1)' : 'none')};

  &:not(:last-child) {
    border-bottom: 1px solid lightgray;
  }
`;
