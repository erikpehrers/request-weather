import React from 'react';
import styled from '@emotion/styled';

const CityName = styled.h2`
  font-size: 4rem;
  text-align: center;
  margin-bottom: 0;
`;

const Temperature = styled.h3`
  font-size: 3rem;
  text-align: center;
`;

const WeatherResults = ({ temp, city }) => {
  return (
    <div>
      <CityName>{city}</CityName>
      <Temperature>{temp}&#8451;</Temperature>
    </div>
  );
};

export default WeatherResults;
