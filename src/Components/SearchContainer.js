import React from 'react';
import styled from '@emotion/styled';
import Downshift from 'downshift';
import { MdSearch as SearchIcon, MdClose as ResetSearch } from 'react-icons/md';
import Icon from './styled/Icon';
import { Dropdown, DropdownItem } from './styled/DropDown';

const SearchWrapper = styled.div`
  width: 100%;
  color: ${({ theme }) => theme.secondary};
  background: white;

  > div {
    width: 100%;
    display: grid;
    grid-template-areas:
      'search input close'
      'result result result';
  }
`;

const SearchInput = styled.input`
  width: 100%;
  border: 0;
  line-height: 65px;
  height: 65px;
  outline: 0;
  font-size: 3rem;
  letter-spacing: 1px;
  color: ${({ theme }) => theme.primary};
  font-weight: ${({ theme }) => theme.fontBold};
  grid-area: input;
`;

const Status = styled.div`
  color: red;
`;

const cities = [
  'Stockholm',
  'Göteborg',
  'Edsbyn',
  'Uppsala',
  'Malmö',
  'Kiruna'
];

const matchCity = (search = '', cities) => {
  const term = new RegExp(search, 'i');
  const matches = cities.filter(city => city.match(term));
  if (!matches.length) {
    return [];
  } else {
    return matches;
  }
};

const SearchContainer = ({ handleSearchTerm, handleSearch }) => {
  return (
    <SearchWrapper>
      <Downshift
        onChange={selected => {
          handleSearch(selected);
        }}
      >
        {({
          getInputProps,
          getMenuProps,
          getItemProps,
          inputValue,
          clearSelection,
          isOpen,
          highlightedIndex
        }) => (
          <div>
            <Icon placement={'search'}>
              <SearchIcon />
            </Icon>
            <SearchInput
              {...getInputProps({
                onChange: () => handleSearchTerm(inputValue),
                onBlur: () => handleSearchTerm(''),
                type: 'text'
              })}
            />
            <Icon placement={'close'}>
              <ResetSearch onClick={clearSelection} />
            </Icon>
            <Dropdown {...getMenuProps()}>
              {isOpen && inputValue.length > 0
                ? matchCity(inputValue, cities).map((city, index) => (
                    <DropdownItem
                      {...getItemProps({
                        item: city,
                        key: index,
                        index,
                        isHighlighted: highlightedIndex === index
                      })}
                    >
                      {city}
                    </DropdownItem>
                  ))
                : null}
            </Dropdown>
          </div>
        )}
      </Downshift>
    </SearchWrapper>
  );
};

export default SearchContainer;
