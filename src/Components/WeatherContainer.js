import React, { useState } from 'react';
import SearchContainer from './SearchContainer';
import WeatherResults from './WeatherResults';

const WeatherContainer = () => {
  const [term, handleSearchTerm] = useState('');
  const [result, setResult] = useState({});

  const handleSearch = text => {
    const url = `https://api.openweathermap.org/data/2.5/weather?q=${text},se&units=metric&appid=015d90162735c919b84a9d8904e84a39`;
    const fetchWeather = async url => {
      // await promise
      const data = await fetch(url, {
        mode: 'cors'
      }).then(response => response.json());
      // set to state
      setResult(data);
    };
    // exec async fetch function
    fetchWeather(url);
  };

  const { main, name } = result;
  console.log(result);
  return (
    <section>
      <div>
        <SearchContainer
          handleSearchTerm={handleSearchTerm}
          handleSearch={handleSearch}
        />
      </div>
      {main && main.temp ? (
        <div>
          <WeatherResults city={name} temp={parseInt(main.temp)} />
        </div>
      ) : null}
    </section>
  );
};

export default WeatherContainer;

//015d90162735c919b84a9d8904e84a39
