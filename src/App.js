import React from 'react';
import { Global } from '@emotion/core';
import theme, { styleReset } from './Components/Theme';
import { ThemeProvider } from 'emotion-theming';
import styled from '@emotion/styled';
import WeatherContainer from './Components/WeatherContainer';

const AppContainer = styled.div`
  margin: 4rem auto;
  padding: 20px 0;
  max-width: 60%;
  background: rgba(255, 255, 255, 0.7);
  box-shadow: 0 14px 28px rgba(0, 0, 0, 0.25), 0 10px 10px rgba(0, 0, 0, 0.22);
  border-radius: 5px;
  background: white;
  ${({ theme }) => `${theme.media[0]} 
    max-width: 100%;
    margin: 0;
    padding: 6px;
  }`}
`;

const App = () => {
  return (
    <ThemeProvider theme={theme}>
      <AppContainer>
        <Global styles={{ styleReset }} />
        <WeatherContainer />
      </AppContainer>
    </ThemeProvider>
  );
};

export default App;
